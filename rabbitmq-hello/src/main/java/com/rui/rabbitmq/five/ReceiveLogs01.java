package com.rui.rabbitmq.five;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.rui.utils.RabbitMqUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReceiveLogs01 {

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqUtil.getNewConnection();
        Channel channel = connection.createChannel();

        //声明一个交换机
        channel.exchangeDeclare("logs","fanout");

        //声明一个临时队列
        String queue = channel.queueDeclare().getQueue();

        //绑定交换机与队列
        channel.queueBind(queue,"logs","");

        System.out.println("等待接收消息，把接收消息打印在屏幕上....");

        DeliverCallback deliverCallback = (consumerTag , message) -> {
            System.out.println("01控制台打印接收到的消息：" + new String(message.getBody(),"UTF-8"));
        };

        channel.basicConsume(queue,true,deliverCallback,consumerTag -> {});
    }
}
