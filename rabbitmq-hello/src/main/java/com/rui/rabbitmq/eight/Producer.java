package com.rui.rabbitmq.eight;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rui.utils.RabbitMqUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

//死信队列 之 生产者代码
public class Producer {

    public static final String NORMAL_EXCHANGE = "normal_exchange";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqUtil.getNewConnection();
        Channel channel = connection.createChannel();

        //死信消息 设置TTL时间
//        AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();

        for (int i = 1; i < 11 ; i++){
            String message = "info" + i;
            channel.basicPublish(NORMAL_EXCHANGE,"zhangsan",null,message.getBytes());
        }
    }
}
