package com.rui.rabbitmq.seven;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.rui.utils.RabbitMqUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

//消费者C1
public class RecieveLogsTopic01 {

    public static final String EXCHANGE_NAME = "topic_logs";

    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = RabbitMqUtil.getNewConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME,"topic");

        //声明队列
        String queueName = "Q1";
        channel.queueDeclare(queueName,false,false,false,null);

        channel.queueBind(queueName,EXCHANGE_NAME,"*.orange.*");
        System.out.println("等待接收消息....");

        DeliverCallback deliverCallback = (consumerTag , message) -> {
            System.out.println(new String(message.getBody(),"utf-8"));
            System.out.println("接收队列：" + queueName + "绑定键：" + message.getEnvelope().getRoutingKey());
        };
        //接收消息
        channel.basicConsume(queueName,true,deliverCallback,consumerTag -> {});
    }
}
